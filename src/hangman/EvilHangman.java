package hangman;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class EvilHangman implements IEvilHangmanGame {

	int guessesRemaining;
	Partition currentPartition;
	File dictionary;
	int wordLength;
	TreeSet<Character> guessedLetters;
	PartialWord partialWord;
	Key currentKey;
	Scanner in;
	
	public EvilHangman()
	{

	}
	
	@Override
	public void startGame(File dictionary, int wordLength)
	{
		in = new Scanner(System.in);
		guessesRemaining = Integer.MAX_VALUE;
		currentKey = new Key(wordLength);
		guessedLetters = new TreeSet<Character>();
		this.dictionary = dictionary;	
		this.wordLength = wordLength;
		currentPartition = new Partition(currentKey);
		partialWord = new PartialWord(currentKey);
		try {
			parseDictionary();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("There was an error with the dictionary.");
			System.out.println(e.toString());
		}
	}

	@Override
	public Set<String> makeGuess(char guess) throws GuessAlreadyMadeException {
		
		if(letterAlreadyGuessed(guess))
		{
			throw new GuessAlreadyMadeException();
		}
		else
		{
			Partitions temp = new Partitions(currentPartition, guess);
			currentPartition = temp.getBestPartition();
//			System.out.println("Current key: " + currentPartition.getKey().getKey());
			currentKey = currentPartition.getKey();
			partialWord.mergeInKey(currentKey);
//			System.out.println("Current Partial Word: " + partialWord.toString());
		}
		
		return currentPartition.getList();
	}
		
	public String getPartialWord()
	{
		return partialWord.toString();
	}
	
	private boolean letterAlreadyGuessed(char c)
	{
		Character temp = new Character(c);
		for(Character ch : guessedLetters)
		{
			if(ch.charValue() == temp.charValue())
			{
				return true;
			}
		}
		
		guessedLetters.add(c);
		return false;
	}
	
	private void parseDictionary() throws Exception
	{
		Scanner scan;
		try {
			scan = new Scanner(new BufferedReader(new FileReader(dictionary)));

			if(!scan.hasNext()) // empty dictionary
			{
				throw new Exception("The dictionary appears to be empty...");
			}
			
			while(scan.hasNext())
			{
				String word = scan.next();
				if(word.length() == wordLength)
				{
					word.toLowerCase();
					if(word.matches("[a-z]+"))
					{
						currentPartition.addWord(word);						
					}
					else
					{
						System.out.println("Invalid word in Dictionary: " + word);
					}
				}		
			}
			
			if(currentPartition.getSize() == 0)
			{
				throw new Exception("The dictionary file did not contain any words of the length you specified. Please try again later.");
			}
			
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		
	}
	
	public String getGuessedLetters()
	{
		String out = "";
		
		for(Character c : guessedLetters)
		{
			out += c.toString() + " ";
		}
		
		out.trim();
		return out;
	}
	
	public void playGame(int guess)
	{
		guessesRemaining = guess;
		
		while(guessesRemaining > 0)
		{
			System.out.println("\nRemaining Guesses: " + guessesRemaining);
			System.out.println("Letters Guessed: " + getGuessedLetters());
			System.out.println(getPartialWord());
			System.out.println("Please guess a letter!");
			System.out.println("Guess: ");
			String input = in.nextLine();
			input = input.toLowerCase();
			
			
			if(input.length() == 0 || input == null)
			{
				System.out.println("ERROR. Please choose a letter to guess.");
				System.out.println("Please guess a letter!");
				System.out.println("Guess: ");
				input = in.nextLine();
				input = input.toLowerCase();
				
				if(input.length() == 0 || input == null)
				{
					System.out.println("You need to learn how to type a letter before playing this game");
					return;
				}
			}
			
			if(input.charAt(0) < 'A' || input.charAt(0) > 'z' || (input.charAt(0) > 'Z' && input.charAt(0) < 'a'))
			{
				System.out.println("ERROR. You must choose a letter. No other character is accepted.");
				System.out.println("Please guess a letter!");
				System.out.println("Guess: ");
				input = in.nextLine();
				input = input.toLowerCase();
				
				if(input.charAt(0) < 'A' || input.charAt(0) > 'z' || (input.charAt(0) > 'Z' && input.charAt(0) < 'a'))
				{
					System.out.println("Please go learn what a letter is before trying to play this game again.");
					return;
				}
			}
				
			if(input.length() > 1)
			{
				System.out.println("Please enter only 1 character.");
				System.out.println("Please guess a letter!");
				System.out.println("Guess: ");
				input = in.nextLine();
				input = input.toLowerCase();
				
				if(input.length() > 1)
				{
					System.out.println("Please go learn to count to 1 before trying to play this game again.");
					return;
				}	
			}
			
			input.toLowerCase();
			int wordSize = partialWord.numLetters();
			try {
				makeGuess(input.charAt(0));
			} catch (GuessAlreadyMadeException e) {
				System.out.println("Hey! You already guessed that letter!");
				System.out.println("Please guess a letter!");
				System.out.println("Guess: ");
				input = in.nextLine();
				input = input.toLowerCase();
				try {
					makeGuess(input.charAt(0));
				} catch (GuessAlreadyMadeException e1) {
					// TODO Auto-generated catch block
					System.out.println("Pay better attention next time! Don't pick letters you've already used!");
					e1.printStackTrace();
					return;
				}
			}
			
			if(partialWord.numLetters() == wordSize) // letter not found in word
			{
				System.out.println("Sorry, there were no " + input.charAt(0) + "\'s");
				guessesRemaining--;
			}
			else
			{
				System.out.println("Yes, the word has " + (partialWord.numLetters() - wordSize) + " " + input.charAt(0) + "\'(s)");
			}
			
			if(partialWord.numLetters() == wordLength)
			{
				// victory
				System.out.println(partialWord.toString());
				System.out.println("Congratulations! You win!\nCome back and play again soon!");
				return;
			}
			
		}	
		String final_word = currentPartition.getWordAt(0);
		System.out.println("The word was " + final_word);
		System.out.println("Looks like you lost to a dumb ol' machine.\nCome back and play again soon!");
		return;
	}

}
