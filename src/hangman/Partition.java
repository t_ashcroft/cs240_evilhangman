package hangman;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class Partition implements Comparable
{

	Set<String> partition;
	Key key;
	
	public Partition(Key key)
	{
		this.key = key;
		partition = new HashSet<String>();
	}
	
	public void addWord(String word)
	{
		partition.add(word);
	}
	
	public Set<String> getList()
	{
		return partition;
	}
	
	public int getSize()
	{
		return partition.size();
	}
	
	public Key getKey()
	{
		if(key == null)
		{
			System.out.println("Regenerating key...");
			key = keyRegen();
			
		}
		
		return key;
	}
	
	private Key keyRegen()
	{
		int count = 0;
		String word1 = null;
		String word2 = null;
		String word3 = null;
		if(partition.size() > 2)
		{
			for(String s : partition)
			{
				if(count == 0)
				{
					word1 = s;
				}
				if(count == 1)
				{
					word2 = s;
				}
				if(count == 2)
				{
					word3 = s;
					break;
				}
			}
			
			char c1 = (Character) null;
			char c2 = (Character) null;
			
			for(int i = 0; i < word1.length(); i++)
			{
				if(word1.charAt(i) == word2.charAt(i) && word1.charAt(i) == word3.charAt(i))
				{
					if(c1 == (Character) null)
					{
						c1 = new Character(word1.charAt(i));
					}
					
					if(c1 != (Character) null && c2 == (Character) null)
					{
						if(word1.charAt(i) != word2.charAt(i) || word1.charAt(i) != c1)
						{
							c2 = new Character(word1.charAt(i));
						}
					}
				}
			}
			
			if(c1 != (Character) null)
			{
				return new Key(word1, c1);
			}
		}
		System.out.println("No key could be generated....");
		return null;
	}
	
	public String getKeyString()
	{
		return key.getKey();
	}
	
	public String getWordAt(int index)
	{
		String out;

	//	String[] temp = (String[]) partition.toArray();
		Object[] temp = partition.toArray();
		out = temp[index].toString();
		return out;
	}

	@Override
	public int compareTo(Object o) {
		
		Partition other = (Partition) o;
		
		if(key.numLetters() < other.getKey().numLetters())
		{
			return -1;
		}
		else if(key.numLetters() > other.getKey().numLetters())
		{
			return 1;
		}
		else if(key.numLetters() == other.getKey().numLetters())
		{
			return key.toString().compareTo(other.getKey().toString());			
		}
		
		return 0;
	}
	

}
