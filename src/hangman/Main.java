package hangman;

import java.io.File;

public class Main {

	private static void processCL(String[] args) throws Exception
	{
		if(args.length != 3)
		{
			// wrong number of args
			throw new Exception("Args are Dictionary, wordLen, Guesses. No more, no less.");
		}
		
		// Dictionary error checking
		File dict = new File(args[0]);
		if(!dict.exists())
		{
			throw new Exception("ERROR. Dictionary does not exist in specified path.");
		}
		if(!dict.isFile())
		{
			throw new Exception("ERROR. The Dictionary path does not point to a file.");
		}
		if(!dict.canRead())
		{
			throw new Exception("ERROR. The Dictionary file cannot be read. Please change the file permissions.");
		}
		
		// wordLen error checking
		String length = args[1];
		int wordLength;
		try{
			wordLength = Integer.parseInt(length);
		}catch(NumberFormatException e){
	        throw new Exception("The Word Length was not a number.");
	    }
		if(wordLength < 2)
		{
			throw new Exception("Word length must be 2 or more");
		}
		
		// num guesses error checking
		String guess = args[2];
		int numGuesses;
		try
		{
			numGuesses = Integer.parseInt(guess);
		}
		catch(NumberFormatException e)
		{
			throw new Exception("The number of guesses was not a number");
		}
		if(numGuesses < 1)
		{
			throw new Exception("The number of guesses must be 1 or more.");
		}
	}
	
	public static void main(String[] args) {
		
		// args are: dictionary wordLen guesses
		try
		{
			processCL(args);
			EvilHangman game = new EvilHangman();
			game.startGame(new File(args[0]), Integer.parseInt(args[1]));
			game.playGame(Integer.parseInt(args[2]));			
		
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}

	}

}
