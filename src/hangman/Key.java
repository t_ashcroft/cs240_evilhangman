package hangman;

public class Key {

	StringBuilder sb;
	int length;
	String key;
	
	public Key(int len)
	{
		sb = new StringBuilder();
		length = len;
		for(int i = 0; i < len; i++)
		{
			sb.append("-");
		}
		key = sb.toString();
	}
	
	private void updateKey()
	{
		key = sb.toString();
	}

	public String getKey()
	{	
		return key;
	}
	
	public String toString()
	{
		return key;
	}
	
	public Key(String word, char c)
	{
		StringBuilder sb = new StringBuilder();
		length = word.length();
		for(int i = 0; i < length; i++)
		{
			if(word.charAt(i) == c)
			{
				sb.append(c);
			}
			else
			{
				sb.append('-');
			}
		}
		
		key = sb.toString();
	}
	
	public String mergeKey(String othKey)
	{
		sb = new StringBuilder(key);
		StringBuilder sb1 = new StringBuilder(othKey);
		StringBuilder out = new StringBuilder();
		for(int i = 0; i < sb.length(); i++)
		{
			if(sb.charAt(i) == '-' && sb1.charAt(i) != '-')
			{
				out.append(sb1.charAt(i));
			}
			else if(sb.charAt(i) != '-' && sb1.charAt(i) == '-')
			{
				out.append(sb.charAt(i));
			}
			else if(sb.charAt(i) == '-' && sb1.charAt(i) == '-')
			{
				out.append('-');
			}
			
		}
		
		sb = out;
		updateKey();
		return key;
	}
	
	public int numLetters()
	{
		int count = 0;
		for(int i = 0; i < key.length(); i++)
		{
			if(key.charAt(i) != '-')
			{
				count++;
			}
		}
		
		return count;		
	}
	
}
