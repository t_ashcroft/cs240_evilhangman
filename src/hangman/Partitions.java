package hangman;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

public class Partitions{

	Set<Partition> partitions;
	
	public Partitions(Partition p, char c)
	{
		partitions = new TreeSet<Partition>();	
		
		Set<String> temp = p.getList();
		Map<String, Partition> temp_partitions = new HashMap<String, Partition>(); // 		Map<Key, Partition> temp_partitions = new HashMap<Key, Partition>();
		for(String s : temp)
		{
			Key key = new Key(s, c);
			
			if(temp_partitions.containsKey(key.toString()))
			{
				temp_partitions.get(key.toString()).addWord(s);
			}
			else
			{
				temp_partitions.put(key.toString(),  new Partition(key));
				temp_partitions.get(key.toString()).addWord(s);
			}
		}
		
		for(String k : temp_partitions.keySet())
		{
			partitions.add(temp_partitions.get(k));
		}
		
	}
	
	public Partition getBestPartition()
	{
		Partition out = null;
		int large = 0;
		Vector<Partition> parts = new Vector<Partition>();
		
		for(Partition p : partitions)
		{
			if(p.getSize() > large)
			{
				large = p.getSize();
				parts.clear();
				parts.add(p);
			}
			else if(p.getSize() == large)
			{
				parts.add(p);
			}
		}

		// only one best set
		if(parts.size() == 1)
		{
			return parts.get(0);
		}
		else
		{
			parts = leastChars(parts);
			if(parts.size() == 1)
			{
				return parts.get(0);
			}
			else
			{
				// find char furthest right
				out = furthestRight(parts);
			}

		}
		return out;
	}
	
	private Partition furthestRight(Vector<Partition> parts)
	{
		Vector<Partition> temp = new Vector<Partition>();
		Partition temp_part = null;
		for(Partition p : parts)
		{
			if(temp_part == null)
			{
				temp_part = p;
			}
			
			if(p.getKey().getKey().compareTo(temp_part.getKey().getKey()) == -1)
			{
				temp_part = p;
			}
		}
		
		
//		for(int i = parts.get(0).getSize(); i > 0; i--)
//		{
////			temp.clear();
////			for(Partition p: parts)
////			{
////				if(p.getKey().toString().charAt(i) != '-')
////				{
////					temp.add(p);
////				}				
////			}
////			
////			if(temp.size() == 1)
////			{
////				return temp.get(0);
////			}
//////			if(temp.size() > 1)
//////			{
//////				temp.clear();
//////			}			
//		}
		
//		if(temp.size() == 0)
//		{
//			System.out.println("Furthest Right Partition error.");
//		}
		
		return temp_part; //previously return temp.get(0) if it gets to the end and hasn't found the best, return first Partition.
	}
	
	private Vector<Partition> leastChars(Vector<Partition> parts)
	{
		Vector<Partition> out = new Vector<Partition>();
		int curr_max = Integer.MAX_VALUE;
		
		for(Partition p : parts)
		{
			if(p.getKey().numLetters() < curr_max)
			{
				out.clear();
				curr_max = p.getKey().numLetters();
				out.add(p);
			}
			else if(p.getKey().numLetters() == curr_max)
			{
				out.add(p);
			}
		}
		
		return out;
	}
	
	public Set<Partition> getPartitions()
	{
		return partitions;
	}

	
}
