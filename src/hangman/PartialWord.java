package hangman;

public class PartialWord {

	String partial;
	
	public PartialWord(String word)
	{
		partial = word;
	}
	
	public void setPartialWord(String word)
	{
		partial = word;
	}
	
	public PartialWord(Key key)
	{
		partial = key.getKey();
	}
	
	public boolean wordCompleted()
	{
		for(int i = 0; i < partial.length(); i++)
		{
			if(partial.charAt(i) == '-')
			{
				return false;
			}
		}
		return true;
	}
	
	public String mergeInKey(Key key)
	{
		StringBuilder sb_key = new StringBuilder(key.getKey());
		StringBuilder pw = new StringBuilder(partial);
		StringBuilder out = new StringBuilder();
		for(int i = 0 ; i < pw.length(); i++)
		{
			if(sb_key.charAt(i) > '-')
			{
				out.append(sb_key.charAt(i));
			}
			else if(pw.charAt(i) > '-')
			{
				out.append(pw.charAt(i));
			}
			else
			{
				out.append('-');
			}
		}
		
		partial = out.toString();
		return partial;
	}
	
	public int numLetters()
	{
		int out = 0;
		
		for(int i = 0; i < partial.length(); i++)
		{
			if(partial.charAt(i) != '-')
			{
				out++;
			}
		}
		return out;
	}
	
	public String toString()
	{
		return partial;
	}
	
}
